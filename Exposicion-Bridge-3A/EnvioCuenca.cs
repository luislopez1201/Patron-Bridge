﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //Clase de envios a Cuenca la cual debe implementar los metodos indicados en la interfaz EnvioProductos.
    public class EnvioCuenca : IEnvioProductos
    {
        //Metodos que indican el proceso, envio y entrega del producto hacia Cuenca.
        public string ProocesarPedidos()
        {
            return "Pedido procesado Cuenca";
        }
        public string Envio()
        {
            return "Pedido enviado a Cuenca";
        }
        public string Entrega()
        {
            return "Pedido entregado a Cuenca";
        }
    }
}