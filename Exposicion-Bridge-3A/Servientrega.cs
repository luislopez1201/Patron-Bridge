﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //clase abstracta Servientrega que permitira implementar los metodos a la clase ReparticionServientrega.
    public abstract class Servientrega
    {
        //Atributo protegido llamado iEnvioProductos.
        protected IEnvioProductos iEnvioProductos;
        //Constructor al que se le pasa como parametro un objeto de tipo IEnvioProductos que puede ser de cualquier cuidad registrada.
        public Servientrega(IEnvioProductos Envio)
        {
            iEnvioProductos = Envio;
        }
        //Metodo para procesar el pedido de cualquier clase que implemente iEnvioProductos.
        public string ProcesarPedido()
        {
            return iEnvioProductos.ProocesarPedidos();
        }
        //Metodo para registrar la cuidad a la cual se enviara el producto.
        public string EnvioDePaquetes()
        {
            return iEnvioProductos.Envio();
        }
        //Metodo para registrar la cuidad donde se entregara el producto.
        public string EntregaPaquetes()
        {
            return iEnvioProductos.Entrega();
        }
        //Metoto para asigar el envio del producto.
        public void AsignarEnvio(IEnvioProductos Envio)
        {
            iEnvioProductos = Envio;
        }
        //Netodo para obtener el lugar de envio del producto.
        public IEnvioProductos ObtenerEnvio()
        {
            return iEnvioProductos;
        }
    }
}