﻿using System;

namespace Exposicion_Bridge_3A
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Añadimos un nuevo objeto el cual por defecto empezara por el envio hacia Manta.
            //Podemos añadir ciertos parametros por ejemplo el ID del pedido.
            ReparticionServientrega EntregaProducto = new ReparticionServientrega("1452");

            Console.WriteLine(EntregaProducto.ProcesarPedido());
            Console.WriteLine(EntregaProducto.EnvioDePaquetes());
            Console.WriteLine(EntregaProducto.EntregaPaquetes());
            Console.WriteLine(EntregaProducto.MostrarIdPedido());

            Console.ReadLine();
            //De esta manera podemos asignar un nuevo destino para realizar el envio que en este caso seria Quito.
            EntregaProducto.AsignarEnvio(new EnvioQuito());
            Console.WriteLine(EntregaProducto.ProcesarPedido());
            Console.WriteLine(EntregaProducto.EnvioDePaquetes());
            Console.WriteLine(EntregaProducto.EntregaPaquetes());
            Console.WriteLine(EntregaProducto.MostrarIdPedido());
        }
    }
}
