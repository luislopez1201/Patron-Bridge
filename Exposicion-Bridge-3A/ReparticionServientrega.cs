﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //Clase que implementa los metodos de la clase abstracta de Servientrega.
    public class ReparticionServientrega : Servientrega
    {
        //propiedad privada que almacena el id del pedido.
        private string NumeroDelPedido { get; set; }
        //Constructor que le pasamos el idPedido llama por defecto a la clase base EnvioManta
        //Crea un nuevo objeto de EnvioManta.
        public ReparticionServientrega(string idPedido):base(new EnvioManta())
        {
            this.NumeroDelPedido = idPedido;
        }
        //Constructor por si quisieramos mostrar el id del pedido y ademas la ciudad de envio.
        public ReparticionServientrega(string idPedido, IEnvioProductos Envio):base(Envio)
        {
            this.NumeroDelPedido = idPedido;
        }
        //Metodo para motrar el Número de pedido.
        public string MostrarIdPedido()
        {
            return "Su número de pedido es: "+ this.NumeroDelPedido;
        }
    }
}