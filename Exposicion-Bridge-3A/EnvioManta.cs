﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //Clase de envios a Manta la cual debe implementar los metodos indicados en la interfaz EnvioProductos.
    public class EnvioManta : IEnvioProductos
    {
        //Metodos que indican el proceso, envio y entrega del producto hacia Manta.
        public string ProocesarPedidos()
        {
            return "Pedido procesado Manta";
        }
        public string Envio()
        {
            return "Pedido enviado a Manta";
        }
        public string Entrega()
        {
            return "Pedido entregado a Manta";
        }
    }
}