﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //Interfaz de EnvioProductos la cual nos permitira aplicar de manera obligatoria los metodos introducidos a las clases que herede.
    public interface IEnvioProductos
    {
        //Netodos para procesar el registro de pedidos.
        string ProocesarPedidos();
        //Metodo para registrar el lugar de envio.
        string Envio();
        //Metodo para indicar el lugar de entrega del producto.
        string Entrega();
    }
}