﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exposicion_Bridge_3A
{
    //Clase de envios a Quito la cual debe implementar los metodos indicados en la interfaz EnvioProductos.
    public class EnvioQuito : IEnvioProductos
    {
        //Metodos que indican el proceso, envio y entrega del producto hacia Quito.
        public string ProocesarPedidos()
        {
            return "Pedido procesado Quito";
        }
        public string Envio()
        {
            return "Pedido enviado a Quito";
        }
        public string Entrega()
        {
            return "Pedido entregado a Quito";
        }
    }
}